class Request < ActiveRecord::Base
  has_many :markers, as: :markable
end
