require 'geocoder'

class Marker < ActiveRecord::Base
  belongs_to :markable, polymorphic: true
  extend Geocoder::Model::ActiveRecord
  reverse_geocoded_by :lat, :long
end
