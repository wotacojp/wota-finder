require_relative '../models/shower'

Shower.create!(name: 'Wota Shower 1', markers: [Marker.create!(lat: 35.70717019821377, long: 139.7614681720734)])
Shower.create!(name: 'Wota Shower 2', markers: [Marker.create!(lat: 35.702265232154964, long: 139.76772308349612)])
