class CreateShowersTable < ActiveRecord::Migration[5.2]
  def change
    create_table :showers do |t|
      t.string :name
      t.timestamps
    end
  end
end
