class AddIndexToMarkers < ActiveRecord::Migration[5.2]
  def change
    add_index :markers, [:lat, :long]
  end
end
