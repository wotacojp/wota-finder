class CreateMarkerTable < ActiveRecord::Migration[5.2]
  def change
    create_table :markers do |t|
      t.decimal   :lat, precision: 14, scale: 10
      t.decimal   :long, precision: 14, scale: 10
      t.string    :name
      t.integer   :markable_id
      t.string    :markable_type
      t.timestamps
    end
  end
end
