function search(searchLat, searchLong, showers, requests) {
  var map = null;
  if (searchLat == 0 && searchLong == 0) {
    map = L.map('map').setView([35.681064119300316, 139.76710081100467], 13);
  } else {
    map = L.map('map').setView([searchLat, searchLong], 13);
  }

  var requestIcon = L.icon({
    iconUrl: 'images/shower.png',

    iconSize:     [25,41], // size of the icon
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  });

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmljaHlib3giLCJhIjoiY2ptajI5d3p1MGJjNDNrcG0zcXBjcjA5aCJ9.m_xAKFPxBDJ6nBu_OGctmQ', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoicmljaHlib3giLCJhIjoiY2ptajI5d3p1MGJjNDNrcG0zcXBjcjA5aCJ9.m_xAKFPxBDJ6nBu_OGctmQ'
  }).addTo(map);

  $(showers).each(function(i,m) { 
    var marker = L.marker([m[0], m[1]]).addTo(map);
  });

  $(requests).each(function(i,m) { 
    var marker = L.marker([m[0], m[1]], {icon: requestIcon}).addTo(map);
  });

  var cloudmadeAttribution = 'Map data &copy; 2011 OpenStreetMap contributors, Imagery &copy; 2011 CloudMade';
  var cloudmade = new L.TileLayer('http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png', {attribution: cloudmadeAttribution});

  var osmGeocoder = new L.Control.OSMGeocoder();

  map.addControl(osmGeocoder);
}
