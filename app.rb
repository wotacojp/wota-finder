require 'dotenv/load'
require 'sinatra'
require "sinatra/activerecord"
require_relative 'models/request'
require_relative 'models/marker'

set :database_file, "config/database.yml"
set :public_folder, File.dirname(__FILE__) + '/static'

get '/' do
  haml :index
end

post '/request-bath' do
  puts params
  request = Request.new
  marker = Marker.new(lat: params[:lat], long: params[:long])
  request.markers << marker
  request.save!
end

get '/search' do
  showers = []
  requests = []
  if !params[:lat].nil? and !params[:long].nil?
    showers = Marker.near([params[:lat], params[:long]], 10).where(markable_type: "Shower").limit(20).to_a.collect {|m| [m.lat, m.long]}
    requests = Marker.near([params[:lat], params[:long]], 10).where(markable_type: "Request").limit(20).to_a.collect {|m| [m.lat, m.long]}
  end
  haml :search, locals: {params: params, showers: showers, requests: requests}
end
